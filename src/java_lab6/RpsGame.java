package java_lab6;

import java.util.Random;

public class RpsGame {
	private int winCount = 0;
	private int tieCount = 0;
	private int lossCount = 0;
	private Random rand = new Random();
	
	public int getWinCount() { return winCount; }
	public int getTieCount() { return tieCount; }
	public int getLossCount() { return lossCount; }
	
	public String playRound(String playerChoice) 
	{
		String computerChoice = generateComputerChoice();
		
		if(playerChoice.equals(computerChoice)) { return draw(computerChoice); }
		
		switch(playerChoice.toLowerCase())
		{
			case "rock":
				if(computerChoice.equals("paper")) { return lose(computerChoice); }
				else { return win(computerChoice); }
			case "paper":
				if(computerChoice.equals("scissors")) { return lose(computerChoice); }
				else { return win(computerChoice); }
			case "scissors":
				if(computerChoice.equals("rock")) { return lose(computerChoice); }
				else { return win(computerChoice); }
			default:
				return null;
		}
	}
	
	private String generateComputerChoice()
	{
		int choiceInt = rand.nextInt(3);
		String[] choices = new String[] {"rock", "paper", "scissors"};
		return choices[choiceInt];
	}
	
	private String draw(String computerChoice) 
	{
		tieCount++;
		return "Computer plays " + computerChoice + ". It's a draw";
	}
	
	private String win(String computerChoice) 
	{
		winCount++;
		return "Computer plays " + computerChoice + ". You win";
	}
	
	private String lose(String computerChoice) 
	{
		lossCount++;
		return "Computer plays " + computerChoice + ". You lose";
	}
}
