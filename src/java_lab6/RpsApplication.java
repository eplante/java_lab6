package java_lab6;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
	
	private RpsGame game = new RpsGame();
	
	public void start(Stage stage) 
	{
		Group root = new Group();
		
		HBox buttons = new HBox();
		Button rock = new Button("rock");
		Button paper = new Button("paper");
		Button scissors = new Button("scissors");
		buttons.getChildren().addAll(rock, paper, scissors);
		
		HBox textfields = new HBox();
		TextField welcome = new TextField("Welcome!");
		TextField wins = new TextField("wins: 0");
		TextField losses = new TextField("losses: 0");
		TextField ties = new TextField("ties: 0");
		welcome.setPrefWidth(200);
		textfields.getChildren().addAll(welcome, wins, losses, ties);
		
		VBox overall = new VBox();
		overall.getChildren().addAll(buttons, textfields);
		
		root.getChildren().add(overall);
		
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);
		
		rock.setOnAction(new RpsChoice(welcome, wins, losses, ties, "rock", game));
		paper.setOnAction(new RpsChoice(welcome, wins, losses, ties, "paper", game));
		scissors.setOnAction(new RpsChoice(welcome, wins, losses, ties, "scissors", game));
		
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);
		
		stage.show();
	}
	
	public static void main(String[] args)
	{
		Application.launch(args);
	}
}
