package java_lab6;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> 
{
	private TextField msg, wins, losses, ties;
	private String playerChoice;
	private RpsGame rps;
	
	public RpsChoice(TextField msg, TextField wins, TextField losses, TextField ties, String playerChoice, RpsGame rps)
	{
		this.msg = msg;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.playerChoice = playerChoice;
		this.rps = rps;
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		String roundResult = rps.playRound(playerChoice);
		msg.setText(roundResult);
		wins.setText("Wins: " + rps.getWinCount());
		losses.setText("Losses: " + rps.getLossCount());
		ties.setText("Ties: " + rps.getTieCount());
	}

}
